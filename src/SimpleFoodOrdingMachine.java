import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class SimpleFoodOrdingMachine {
    private JPanel root;
    private JButton tempuraButton;
    private JButton ramenButton;
    private JButton udonButton;
    private JButton SushiButton;
    private JButton KraageButton;
    private JButton YakisobaButton;
    private JLabel topLabel;
    private JButton btncheckout;
    private JLabel Label2;
    private JButton btnhelp;
    private JTextField tftotal;
    private JLabel LbFoodPrice1;
    private JLabel LbFoodPrice2;
    private JLabel LbFoodPrice3;
    private JLabel LBFoodPrice4;
    private JLabel LbFoodPrice5;
    private JLabel LbFoodPrice6;
    private JButton btnDelete;
    private JButton btnclear;
    private JPanel OrderItemList;
    private JTextPane receivedInfo;

    private List<OrderItem> orderList;
    private double totalPrice;

    private class OrderItem {
        String name;
        double price;

        OrderItem(String name, double price) {
            this.name = name;
            this.price = price;
        }
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("SimpleFoodOrderingMachine");
        frame.setContentPane(new SimpleFoodOrdingMachine().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    // ボタンアクションリスナーのメソッド
    void order(String foodName, double price) {
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order " + foodName + "?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);
        if (confirmation == JOptionPane.YES_OPTION) {
            OrderItem newItem = new OrderItem(foodName, price);
            orderList.add(newItem);
            totalPrice += price;
            updateTotalPrice();
            updateOrderItemList();
            receivedInfo.setText(receivedInfo.getText() + "\nOrder for " + foodName + " received");
            JOptionPane.showMessageDialog(null, "Order for " + foodName + " has been added to your order.");
        }
    }

    void updateTotalPrice() {
        tftotal.setText("Total Price : " + totalPrice +" yen ");
    }

    void updateOrderItemList() {
        OrderItemList.removeAll();
        for (OrderItem item : orderList) {
            JLabel itemLabel = new JLabel(item.name + " : " + item.price +  " yen");
            OrderItemList.add(itemLabel);
        }
        OrderItemList.revalidate();
        OrderItemList.repaint();
    }

    void handleCheckout() {
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to checkout?",
                "Checkout Confirmation",
                JOptionPane.YES_NO_OPTION);
        if (confirmation == JOptionPane.YES_OPTION) {
            JOptionPane.showMessageDialog(null, "Thank you. The total price is " + totalPrice +" yen.");
            clearOrders();
        }
    }

    void callHelp() {
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to call for help?",
                "Help Confirmation",
                JOptionPane.YES_NO_OPTION);
        if (confirmation == JOptionPane.YES_OPTION) {
            JOptionPane.showMessageDialog(null, "A staff member is on their way.");
        }
    }

    void deleteLastOrder() {
        if (!orderList.isEmpty()) {
            int confirmation = JOptionPane.showConfirmDialog(null,
                    "Are you sure you want to delete the last order?",
                    "Delete Confirmation",
                    JOptionPane.YES_NO_OPTION);
            if (confirmation == JOptionPane.YES_OPTION) {
                OrderItem lastItem = orderList.remove(orderList.size() - 1);
                totalPrice -= lastItem.price;
                updateTotalPrice();
                updateOrderItemList();
                receivedInfo.setText(receivedInfo.getText() + "\nLast order for " + lastItem.name + " removed");
            }
        }
    }

    void clearOrders() {
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Are you sure you want to clear all orders?",
                "Clear Confirmation",
                JOptionPane.YES_NO_OPTION);
        if (confirmation == JOptionPane.YES_OPTION) {
            orderList.clear();
            totalPrice = 0;
            updateTotalPrice();
            updateOrderItemList();
            receivedInfo.setText("");
        }
    }

    public SimpleFoodOrdingMachine() {
        orderList = new ArrayList<>();
        totalPrice = 0.0;

        // OrderItemListの初期化
        OrderItemList.setLayout(new BoxLayout(OrderItemList, BoxLayout.Y_AXIS));

        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tempura", 500);
            }
        });
        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ramen", 980);
            }
        });
        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Udon", 650);
            }
        });
        SushiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Sushi", 1200);
            }
        });
        KraageButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Karaage", 450);
            }
        });
        YakisobaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Yakisoba", 720);
            }
        });
        btncheckout.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                handleCheckout();
            }
        });
        btnhelp.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                callHelp();
            }
        });
        btnDelete.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                deleteLastOrder();
            }
        });
        btnclear.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                clearOrders();
            }
        });
    }
}
