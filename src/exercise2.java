import java.util.Scanner;
public class exercise2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("What would you like to order:");
        System.out.println("1. Tempura");
        System.out.println("2. Ramen");
        System.out.println("3. Udon");
        System.out.print("Your order [1-3]: ");
        int choice = scanner.nextInt();

        String foodName;
        switch (choice) {
            case 1:
                order("Tempura");
                break;
            case 2:
                order( "Ramen");
                break;
            case 3:
                order("Udon");
                break;
            default:
                System.out.println("Invalid choice.");
                return;
        }

    }

     static void order(String foodName) {
        System.out.println("You have ordered " + foodName + ". Thank you!");
    }
}
